import { CardEntity } from './card.entity';

export const CARDS: CardEntity[] = [
    {
        brandID: 2,
        name: 'Fabien',
        stats: {
            str: 10,
            int: 3,
            blk: 2,
            dod: 5,
        }
    },
    {
        brandID: 1,
        name: 'Jeremy',
        stats: {
            str: 3,
            int: 1,
            blk: 10,
            dod: 6,
        }
    },
    {
        brandID: 1,
        name: 'Pia',
        stats: {
            str: 1,
            int: 10,
            blk: 2,
            dod: 7,
        }
    },
];