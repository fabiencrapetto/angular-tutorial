import { Component, Input, OnInit } from '@angular/core';
import { CardEntity } from '../card.entity';
import { BRANDS } from '../mock-brands';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() card: CardEntity;
  brands = BRANDS;

  constructor() { }

  ngOnInit(): void {
  }

  getBrandName(id: number) {
    return this.brands.filter(brand => {
      if (brand.id === id) return brand;
    })[0].name;
  }

}
