import { Component, Input, OnInit } from '@angular/core';
import { CardEntity } from '../card.entity';

import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-deck',
  templateUrl: './deck.component.html',
  styleUrls: ['./deck.component.scss']
})
export class DeckComponent implements OnInit {
  @Input() mydeck: CardEntity[] = [];
  @Input() mat: CardEntity[] = [];

  constructor() {
  }

  ngOnInit(): void {
    this.mydeck.forEach(card => {
      card.thumb = '/assets/img/' + card.name + '.jpg';
    });
  }
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }
}
