import { Component, Output } from '@angular/core';
import { CARDS } from './mock-cards';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'CLBS Game';
  @Output() mydeck = CARDS;
  @Output() mat = [];
}
