export interface CardEntity {
    brandID: number;
    name: string;
    stats: {
        str: number;
        int: number;
        blk: number;
        dod: number;
    };
    thumb?: string;
}