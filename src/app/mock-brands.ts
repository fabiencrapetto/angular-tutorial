import { Brand } from './brand';

export const BRANDS: Brand[] = [
    {
        id: 1,
        name: 'CLBS',
    },
    {
        id: 2,
        name: 'BSF',
    },
];